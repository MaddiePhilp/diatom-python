"""
    Script to assist with counting diatoms from a slide
    """
#Import os module
try:
    import os
except:
    raise Exception("Could not import os package")

#Import inquirer module
try:
    import inquirer
except:
    raise Exception("Could not import inquirer. Please install using pip install inquirer")

#Import csv module
try:
    import csv
except:
    raise Exception("Could not import csv")

#Welcome message design
print("___________________________________________________________")
print("|                                                         |")
print("|                         Welcome!                        |")
print("|                                                         |")
print("|_________________________________________________________|")
print("|                                                         |")
print("| This software is built to assist with counting diatoms. |")
print("|       It is provided with absolutely no warranty.       |")
print("|                                                         |")
print("|      Maddie Philp - Aberystwyth University - 2018       |")
print("|_________________________________________________________|")

#define required global variables
diatomcount = dict()
output = ""
overallcount = 0

#Define whether resuming or starting new count file
while True:
    print("")
    print("Press a key to Start!")
    print("")
    print("-----------------------------")
    print(" Key   Description")
    print("-----------------------------")
    print("  s    start new count")
    print("  r    resume previous count")
    print("  h    help")
    print("  q    quit")
    print("-----------------------------")
    start = input()
    start = start.lower()
    #Define parameters for new count
    if start == 's':
        print("New analysis started")
        while True:
            #The total number to be counted
            print("")
            print("Please enter the total number of diatoms you wish to count to as an interger: ")
            number = input()
            print(" ")
            print("You entered: " + number)
            print("")
            print("Is this correct? (y/n)")
            correct = input()
            correct = correct.lower()
            #Check input is correct
            if correct == 'y':
                #Confirm can be convereted to interger
                try:
                    number = int(number)
                except:
                    print("Please enter as an interger (e.g. 1) rather than a word (e.g. one)")
                    continue
                print("")
                print("Let's get started!")
                break
            #Option to define different number
            elif correct == 'n':
                print("")
                print("Let's try again!")
            #Handle exceptions
            else:
                print("")
                print("Answer not recognised please answer with Y for yes or N for no.")
                print("")
                print("Let's try again!")
        #Define output location
        while True:
                print("")
                output = input("Please enter output file location and name:")
                print("")
                print("You entered: " + output)
                print("")
                print("Is this correct? (y/n)")
                correct = input()
                correct = correct.lower()
                #check its correct
                if correct == 'y':
                    path, filename  = os.path.split(output)
                    if not os.path.isdir(path):
                        os.makedirs(path)
                    if not '.csv' in filename:
                        output = output + ".csv"
                    print("")
                    print("Output file defined as : " + output)
                    break
                elif correct == 'n':
                    print("")
                    print("Let's try again!")
                #help with inputing file locations
                elif correct == 'h':
                    print("")
                    print("Please enter the path to the file location.")
                    print("Windows this will likely look like: 'C:\\Users\joebloggs\Desktop\Diatomcounts\slide3.csv'")
                    print("Unix this will likely look like: '/Users/joebloggs/Desktop/Diatomcounts/slide3.csv'")
                    print("Be sure to enter as backslashes for windows locations!")
                    print("Also please define the file extention as .csv")
                    print("")
                else:
                    print("")
                    print("Input not recognised please answer with y/n")
                    print("")
                    print("Let's try again!")
        break
    #options to resume previous analysis
    if start == 'r':
        print("Resuming previous analysis")
        while True:
            print("")
            output = input("Path to part-complete csv file: ")
            if not '.csv' in output:
                output = output + ".csv"
            print("")
            print("Output file defined as :" + output)
            if not os.path.isfile(output):
                print("Could not locate file")
            else:
                with open(output,'r') as f:
                    next(f)
                    reader = csv.reader(f, skipinitialspace=True)
                    diatomcount = dict(reader)
                    diatomcount = dict((k,int(v)) for k,v in diatomcount.items())
                    print("___________________________________________________________")
                    print("|                                                         |")
                    print("|                     Current Counts                      |")
                    print("___________________________________________________________")
                    print("")
                    print(diatomcount)
                    print("___________________________________________________________")
                    currenttotal = sum(diatomcount.values())
                    print("The current count total is: " + str(currenttotal))
                    #Define total for count
                    while True:
                        print("")
                        print("Please enter the total number of diatoms you wish to count to as an interger: ")
                        number = input()
                        print(" ")
                        print("You entered: " + number)
                        print("")
                        print("Is this correct? (y/n)")
                        correct = input()
                        correct = correct.lower()
                        if correct == 'y':
                            try:
                                number = int(number)
                            except:
                                print("Please enter as an interger (e.g. 1) rather than a word (e.g. one)")
                                continue
                            print("")
                            print("Let's get started!")
                            #Start with the number that previously got to
                            overallcount +=currenttotal
                            break
                        elif correct == 'n':
                            print("")
                            print("Let's try again!")
                        else:
                            print("")
                            print("Answer not recognised please answer with Y for yes or N for no.")
                            print("")
                            print("Let's try again!")
                break
    #Help information
    if start == 'h':
        print("___________________________________________________________")
        print("|                     Help Information                    |")
        print("|_________________________________________________________|")
        print("|Select species from list of previous or select the       |")
        print("|option to add to the list.                               |")
        print("|                                                         |")
        print("|If not previously counted please select to add new       |")
        print("|species name. Please be sure to spell it correctly!      |")
        print("|                                                         |")
        print("|Next you will be asked the number of this species that   |")
        print("|you wish to record                                       |")
        print("|                                                         |")
        print("|The software will automatically stop and output a csv    |")
        print("|file when the previously defined number of diatoms to    |")
        print("|count has been reached                                   |")
        print("|                                                         |")
        print("|To leave and continue later select quit from prompts     |")
        print("|when prompted if itis the first time you have identified |")
        print("|this species                                             |")
        print("|                                                         |")
        print("|                      Happy Counting!                    |")
        print("___________________________________________________________")
    #Handle quit
    if start == 'q':
        while True:
            print("Are you sure you want to terminate analysis? (y/n)")
            terminate = input()
            terminate = terminate.lower()
            if terminate == 'y':
                print("")
                print("___________________________________________________________")
                print("                   Terminating analysis.")
                print("")
                print("___________________________________________________________")
                number = 0
                overallcount = 0
                break
            elif terminate == 'n':
                print("Resuming Analysis")
                break
    break

#Count loop
while overallcount < int(number):
    print("___________________________________________________________")
    print("")
    print("Diatom Number: " + str(overallcount) + " of " + str(number))
    print("")
    print("___________________________________________________________")
    print("")
    print(diatomcount)
    print("")
    print("___________________________________________________________")
    speciesselection = ""
    #Define new species
    previous = list(diatomcount.keys())
    previous.append("Add Species")
    previous.append("Quit")
    species = [inquirer.List("species", message = "Which species is it?", choices = previous)]
    speciesselctionprompt = inquirer.prompt(species)
    speciesselection = speciesselctionprompt["species"]
    if speciesselection == "Quit":
        print("Are you sure you want to terminate analysis? (y/n)")
        terminate = input()
        terminate = terminate.lower()
        if terminate == 'y':
            print("")
            print("___________________________________________________________")
            print("Terminating analysis.")
            print("To resume later start up and add csv file")
            print("")
            print("___________________________________________________________")
            overallcount +=number
            break
        elif terminate == 'n':
            print("Resuming Analysis")
    else:
        if speciesselection == "Add Species":
            while True:
                print("Please type its name")
                name = input()
                print("Is " + name + " correct? (y/n)")
                correct = input()
                correct = correct.lower()
                if correct == 'y':
                    if name in diatomcount:
                        print("This species has already been counted!")
                        print("Please try again")
                    else:
                        diatomcount.update({name :0})
                        speciesselection = name
                        break
                if correct == 'n':
                    print("Try again")

        print("Adding to count of " + speciesselection)
        if not speciesselection in diatomcount:
            print("")
            print("Could not locate species.")
            print("Please try again.")
        while True:
            print("")
            print(speciesselection)
            print("Add how many?")
            addition = input()
            try:
                addition = int(addition)
            except:
                print("please add as interger")
                continue
            diatomcount[speciesselection] += int(addition)
            overallcount += addition
            break

print("___________________________________________________________")
print("                         Completed!")
print("")
print("Final Counts:")
print("")
print(diatomcount)
print("___________________________________________________________")
print("")
if overallcount == 0:
    print("")
    print("Thank you for using this software!")
    print("___________________________________________________________")
else:
    print("Outputting to CSV File: "+ output)
    with open(output, "w") as f:
        writer = csv.writer(f)
        if not 'Species' in diatomcount.items():
            first = ('Species' , 'Count')
        writer.writerow(first)
        for row in diatomcount.items():
            print(row)
            writer.writerow(row)
    print("___________________________________________________________")
    print("            Thank you for using this software!")
    print("")
    print("___________________________________________________________")

